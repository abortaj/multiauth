<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::post('user/signUp',['uses'=>'UserAuthController@signUp']);
Route::post('user/login',['uses'=>'UserAuthController@login']);

Route::post('admin/signUp',['uses'=>'AdminAuthController@signUp']);
Route::post('admin/login',['uses'=>'AdminAuthController@login']);
