<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Support\Facades\Config;

class UserAuthController extends Controller
{
    public function signUp(Request $request){
        $this->validate($request,[
            'name'=>'required',
            'email'=>'required|email',
            'password'=>'required'
        ]);
        Config::set('jwt.user', 'App\User');
        Config::set('auth.providers.users.model', \App\User::class);
        $user = new User([
            'name'=>$request->input('name'),
            'email'=>$request->input('email'),
            'password'=>bcrypt($request->input('password'))
        ]);
        $user->save();
        return response()->json(['msg'=>'User Created Successfully!'],201);
    }

    public function login(Request $request){
        $this->validate($request,[
            'name'=>'required',
            'email'=>'required|email',
            'password'=>'required'
        ]);
        $credentials = $request->only('email','password');
        try{
            if (! $token = JWTAuth::attempt($credentials)){

                return response()->json(['error'=>'Invalid Credentials!'],401);

            }

        }catch (JWTException $e){
            return response()->json(['error'=>'Could not  create Token!'],401);

        }


        return response()->json(['token'=>$token],200);
    }
}
